package com.example.mobileterminator;

import android.annotation.SuppressLint;
import android.support.v4.app.FragmentActivity;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class Events_SchuelerInfo extends FragmentActivity implements
		ActionBar.TabListener {

	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	public Schueler schueler;
	final String TAG = MainActivity.TAG;

	private ActionBar actionBar;
	// Tab titles
	private String[] tabs = { "Auswahl", "Info", "Anzeige" };
	boolean backpressed = false;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Log.e(TAG, "backpressed-fragmentactivity");
		backpressed = true;
		MainActivity.start = false;
		super.onBackPressed();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		Log.e(TAG, "ONPAUSE-backpressed-fragmentactivity");
		if (backpressed == false) {
			MainActivity.start = true;
		} else {
			MainActivity.start = false;
		}
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.v(TAG, "ONRESUM-backpressed-fragmentactivity!!");
		if (MainActivity.start == true) {
			Intent intent = new Intent(this, StartBildschirm.class);
			startActivity(intent);
		}
		super.onResume();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab);
		MainActivity.start = false;

		Intent intent = getIntent();
		Bundle params = intent.getExtras();
		schueler = (Schueler) params.get("Schueler");
		// Initilization

		actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		mAdapter.sch = schueler;
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(mAdapter);

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

					@Override
					public void onPageSelected(int position) {
						// on changing the page
						// make respected tab selected
						actionBar.setSelectedNavigationItem(position);
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
					}
				});

		// Adding Tabs
		for (int i = 0; i < mAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab().setText(tabs[i])
					.setTabListener(this));
		}
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
