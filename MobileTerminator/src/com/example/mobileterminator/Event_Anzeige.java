package com.example.mobileterminator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class Event_Anzeige extends ListFragment {

	ArrayList<Event> list = new ArrayList<Event>();
	ArrayAdapter<Event> adapter;
	final String TAG = MainActivity.TAG;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_event_anzeige,
				container, false);

		Schueler sch = (Schueler) getArguments().getSerializable("Schueler");
		ListView listview = (ListView) rootView.findViewById(android.R.id.list);
		// list.add(new Event(0, 0, 0, ""));
		list = new ArrayList<Event>();
		adapter = new ArrayAdapter<Event>(
				getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, list);
		setListAdapter(adapter);
		registerForContextMenu(listview);

		// ------------------------------------------------------------------------------------------------

		Calendar cal = Calendar.getInstance();
		DateFormat df_name = new SimpleDateFormat("yyMMdd");
		String filedate = df_name.format(Calendar.getInstance(
				TimeZone.getDefault()).getTime());
		JSONObject json = new JSONObject();
		int value = 0;
		String str = "", s = "";
		JSONArray events;

		String sdState = Environment.getExternalStorageState();
		if (sdState.equals(Environment.MEDIA_MOUNTED)) {
			File outputFile = Environment.getExternalStorageDirectory();

			String path = outputFile.getAbsolutePath();
			String fullname = path + "/" + "KN_" + filedate + ".js";
			File file = new File(fullname);
			if (file.exists()) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(new FileInputStream(fullname)));
					while (reader.ready()) {
						str = reader.readLine().toString();
						s += str;
					}
					// Log.e(TAG, s + "");

					events = new JSONArray(s);
					list = new ArrayList<Event>();
					adapter.notifyDataSetChanged();
					for (int i = 0; i < events.length(); i++) {
						json = events.getJSONObject(i);
						int schueler_id = json.getInt("schueler_id");
						int event_id = json.getInt("Ereignstyp_id");
						int Zus_Wert = json.getInt("Zus_Wert");
						String date = json.getString("Zeitstempel");
						String klasse_id = json.getString("klasse_id");
						Event event = new Event(schueler_id, event_id,
								Zus_Wert, date, klasse_id);
						if (schueler_id == sch.schueler_id) {
							list.add(event);
							// Log.e(TAG, schueler_id + "-"+
							// event_id + " hinzugef�gt");
						}
					}

					boolean schueler_dabei = false;

					for (int i = 0; i < list.size(); i++) {
						Event event = list.get(i);
						if (event.schueler_id == sch.getSchueler_id()) {
							schueler_dabei = true;

						}
					}
					if (schueler_dabei == false) {
						Toast.makeText(
								getActivity().getApplicationContext(),
								"Heute wurden noch keine Ereignisse bei diesem Sch�ler verzeichnet!",
								Toast.LENGTH_LONG).show();
					} else {
						adapter = new ArrayAdapter<Event>(getActivity()
								.getApplicationContext(),
								android.R.layout.simple_list_item_1, list);
						setListAdapter(adapter);
					}

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Toast.makeText(
						getActivity().getApplicationContext(),
						"Heute wurden noch keine gar keine Ereignisse verzeichnet!",
						Toast.LENGTH_LONG).show();
			}
		}

		return rootView;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		getActivity().getMenuInflater().inflate(R.menu.main, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch (id) {
		case R.id.delete:
			ContextMenuInfo info = item.getMenuInfo();
			AdapterContextMenuInfo adapterInfo = (AdapterContextMenuInfo) info;
			int pos = adapterInfo.position;
			Event event = list.get(pos);
			int sch_id = event.schueler_id;
			int e_id = event.event_id;
			int wert = event.Zus_Wert;
			String date1 = event.date;

			JSONObject json = new JSONObject();
			int value = 0;
			String str = "",
			s = "";
			JSONArray events;
			DateFormat df_name = new SimpleDateFormat("yyMMdd");
			String filedate = df_name.format(Calendar.getInstance(
					TimeZone.getDefault()).getTime());
			String sdState = Environment.getExternalStorageState();
			if (sdState.equals(Environment.MEDIA_MOUNTED)) {
				try {
					File outputFile = Environment.getExternalStorageDirectory();

					String path = outputFile.getAbsolutePath();
					String fullname = path + "/" + "KN_" + filedate + ".js";
					File file = new File(fullname);
					BufferedReader reader;

					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(fullname)));

					while (reader.ready()) {
						str = reader.readLine().toString();
						s += str;
					}
					reader.close();
					Log.e(TAG, "first" + s + "");

					events = new JSONArray(s);
					ArrayList<Event> liste = new ArrayList<Event>();
					for (int i = 0; i < events.length(); i++) {
						json = events.getJSONObject(i);
						int schueler_id = json.getInt("schueler_id");
						int event_id = json.getInt("Ereignstyp_id");
						int Zus_Wert = json.getInt("Zus_Wert");
						String date = json.getString("Zeitstempel");
						String klasse_id = json.getString("klasse_id");
						Event event1 = new Event(schueler_id, event_id,
								Zus_Wert, date, klasse_id);
						liste.add(event1);
					}

					JSONArray events1 = new JSONArray();
					for (int i = 0; i < liste.size(); i++) {
						Event currentevent = liste.get(i);

						if (event.toString().equals(currentevent.toString()) == false) {
							JSONObject json1 = new JSONObject();
							json1.put("Zeitstempel", currentevent.date);
							json1.put("Zus_Wert", currentevent.Zus_Wert);
							json1.put("Ereignstyp_id", currentevent.event_id);
							json1.put("schueler_id", currentevent.schueler_id);
							json1.put("klasse_id", currentevent.klasse_id);
							events1.put(json1);
						} else {
							Log.e(TAG,
									"gel�scht: " + currentevent.toString());
						}
					}
					Log.e(TAG, "restliche: " + events1.toString());

					PrintWriter out = new PrintWriter(new OutputStreamWriter(
							new FileOutputStream(fullname)));

					out.print(events1.toString());
					out.flush();
					out.close();

					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(fullname)));
					str = "";
					s = "";
					while (reader.ready()) {
						str = reader.readLine().toString();
						s += str;
					}
					reader.close();
					Log.e(TAG, "second" + s + "");

					events = new JSONArray(s);
					list = new ArrayList<Event>();
					for (int i = 0; i < events.length(); i++) {
						json = events.getJSONObject(i);
						int schueler_id = json.getInt("schueler_id");
						int event_id = json.getInt("Ereignstyp_id");
						int Zus_Wert = json.getInt("Zus_Wert");
						String date = json.getString("Zeitstempel");
						String klasse_id = json.getString("klasse_id");
						if (sch_id == schueler_id) {
							Event event1 = new Event(schueler_id, event_id,
									Zus_Wert, date, klasse_id);
							list.add(event1);
						}
					}
					adapter = new ArrayAdapter<Event>(getActivity()
							.getApplicationContext(),
							android.R.layout.simple_list_item_1, list);
					setListAdapter(adapter);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		return super.onContextItemSelected(item);
	}

	public void readFile() {

	}
}
