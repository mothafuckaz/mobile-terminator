package com.example.mobileterminator;

public enum Event_Auswahl {

	MUENDLICHE_MITARBEITSUEBERPRUEFUNG, 
	SCHRIFTLICHE_MITARBEITSUEBERPRUEFUNG, 
	TEST, 
	SCHULARBEIT, 
	MUENDLICHE_PRUEFUNG, 
	MUENDLICHE_PRUEFUNG_AUF_SCHUELERWUNSCH,
	FESTSTELLUNGSPRUEFUNG,
	SCHUELERIN_FEHLT,
	FRUEHWARNUNG,
	SEMESTERNOTE_FESTGELEGT,
	JAHRESNOTE_FESTGELEGT,
	INFORMATION,
	HAUSUEBUNG_NICHT_GEBRACHT,
	HAUSUEBUNG,
	ZU_SPEAT,
	MITARBEIT; 
}
