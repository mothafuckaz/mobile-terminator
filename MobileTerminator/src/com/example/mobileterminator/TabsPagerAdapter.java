package com.example.mobileterminator;

import android.os.Bundle;
import android.support.v4.app.*;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	Schueler sch;

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		Fragment fragment = null;
		Bundle args = null;
		switch (index) {
		case 0:
			fragment = new EventFragment();
			args = new Bundle();
			args.putSerializable("Schueler", sch);
			fragment.setArguments(args);
			break;

		case 1:
			fragment = new InfoFragment();

			args = new Bundle();
			args.putSerializable("Schueler", sch);
			fragment.setArguments(args);
			break;

		case 2:
			fragment = new Event_Anzeige();

			args = new Bundle();
			args.putSerializable("Schueler", sch);
			fragment.setArguments(args);
			break;

		}

		return fragment;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}
}