package com.example.mobileterminator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class InfoFragment extends Fragment {
	
	final String TAG = MainActivity.TAG;

	public InfoFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_info, container,
				false);

		Schueler sch = (Schueler) getArguments().getSerializable("Schueler");
		// Toast.makeText(getActivity(), sch.toString(),
		// Toast.LENGTH_SHORT).show();
		TextView textview1, textview3, textview5, textview7, textview9, textview11, textview13, textview15;
		textview1 = (TextView) rootView.findViewById(R.id.textView1);
		textview3 = (TextView) rootView.findViewById(R.id.textView3);
		textview5 = (TextView) rootView.findViewById(R.id.textView5);
		textview7 = (TextView) rootView.findViewById(R.id.textView7);
		textview9 = (TextView) rootView.findViewById(R.id.textView9);
		textview11 = (TextView) rootView.findViewById(R.id.textView11);
		textview13 = (TextView) rootView.findViewById(R.id.textView13);
		textview15 = (TextView) rootView.findViewById(R.id.textView15);

		textview1.setText("  " + sch.getName());
		textview3.setText("  " + sch.getKatnr());
		textview5.setText("  " + sch.getFehlt());
		textview7.setText("  " + sch.getFruehgewarnt() + "");
		double prozent = sch.getProzent();
		textview9.setText("  " + sch.getNote() + " (" + sch.getSumplus() + "+/"
				+ sch.getSumminus() + "- = " + (int) prozent + "%)");
		int smplus = sch.getSumminus() - sch.getMuendlich_minus();
		int smminus = sch.getSumplus() - sch.getMuendlich_minus();
		textview11.setText("  " + smplus + "- / " + smminus + "+");
		textview13.setText("  " + sch.getMuendlich_plus() + "+ / "
				+ sch.getMuendlich_minus() + "-");
		textview15.setText("  " + sch.getSemnote() + "");

		return rootView;

	}

}
