package com.example.mobileterminator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class EventFragment extends Fragment {

	String msg;
	int eventid = 0;
	final String TAG = MainActivity.TAG;
	

	public EventFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_event, container,
				false);
		
		final Schueler sch = (Schueler) getArguments().getSerializable(
				"Schueler");
		// Toast.makeText(getActivity(),
		// sch.toString(),Toast.LENGTH_SHORT).show();
		
		final EditText editext = (EditText) rootView
				.findViewById(R.id.editText2);
		TextView name = (TextView) rootView.findViewById(R.id.textview_name);
		name.setText(" " + sch.getName() + "");
		final TextView textview = (TextView) rootView
				.findViewById(R.id.textView1);
		Button button = (Button) rootView.findViewById(R.id.button1);
		textview.setVisibility(View.GONE);
		editext.setVisibility(View.GONE);
		String[] xml = getActivity().getResources().getStringArray(
				R.array.Event_Auswahl);

		final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner1);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(),
				android.R.layout.simple_dropdown_item_1line, xml);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				msg = (String) spinner.getItemAtPosition(position);
				if (msg.equals("ZU SPEAT")) {
					textview.setVisibility(View.VISIBLE);
					editext.setVisibility(View.VISIBLE);
					eventid = 15;
				} else {
					textview.setVisibility(View.GONE);
					editext.setVisibility(View.GONE);
				}
				if (msg.equals("+") || msg.equals("-")) {
					eventid = 1;
				}
				if (msg.equals("SCHUELERIN FEHLT")) {
					eventid = 8;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				boolean eventfehlt = false;
				boolean fehltbereits = false;
				JSONObject json = new JSONObject();
				
				int value = 0;
				if (msg.equals("+")) {
					value = +1;
				}
				if (msg.equals("-")) {
					value = -1;
				}
				if (msg.equals("ZU SPEAT")) {
					if (editext.getText().toString().equals("")) {
					    Toast.makeText(getActivity().getApplicationContext(), "Es wurden keine Minuten eingegeben!", Toast.LENGTH_SHORT).show();
					    return;
					}
					value = Integer.parseInt(editext.getText().toString());
				}
				if (msg.equals("SCHUELERIN FEHLT")) {
					value = 0;
					eventfehlt = true;
				}
				

				Calendar cal = Calendar.getInstance();

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String date = df.format(Calendar.getInstance(
						TimeZone.getDefault()).getTime());
				DateFormat df_name = new SimpleDateFormat("yyMMdd");
				String filedate = df_name.format(Calendar.getInstance(
						TimeZone.getDefault()).getTime());
				// Toast.makeText(getActivity(), date,
				// Toast.LENGTH_SHORT).show();
				try {
					json.put("Zeitstempel", date);
					json.put("Zus_Wert", value);
					json.put("Ereignstyp_id", eventid);
					json.put("schueler_id", sch.getSchueler_id());
					json.put("klasse_id", sch.getKlasse_id());
					Event event = new Event(sch.getSchueler_id(), eventid,
							value, date, sch.getKlasse_id());
					// Toast.makeText(getActivity().getApplicationContext(),json.toString(),
					// Toast.LENGTH_SHORT).show();
					String str = "", s = "";
					JSONArray events;

					String sdState = Environment.getExternalStorageState();
					if (sdState.equals(Environment.MEDIA_MOUNTED)) {
						File outputFile = Environment
								.getExternalStorageDirectory();

						String path = outputFile.getAbsolutePath();
						String fullname = path + "/" + "KN_" + filedate + ".js";
						File file = new File(fullname);
						if (file.exists()) {
							try {
								BufferedReader reader = new BufferedReader(
										new InputStreamReader(
												new FileInputStream(fullname)));
								while (reader.ready()) {
									str = reader.readLine().toString();
									s += str;
								}
								// Log.e(TAG, s + "");
								ArrayList<Event> liste = new ArrayList<Event>();
								events = new JSONArray(s);
								for (int i = 0; i < events.length(); i++) {
									json = events.getJSONObject(i);
									int schueler_id = json
											.getInt("schueler_id");
									int event_id = json.getInt("Ereignstyp_id");
									int Zus_Wert = json.getInt("Zus_Wert");
									String timestamp = json
											.getString("Zeitstempel");
									String klasse_id = json
											.getString("klasse_id");
									Event event1 = new Event(schueler_id,
											event_id, Zus_Wert, timestamp, klasse_id);
									if (schueler_id == sch.schueler_id) {
										if (eventfehlt == true) {
											if (event1.event_id == 15) {
												Log.e(TAG, "zu speat deleted");

											} else {
												liste.add(event1);
												Log.e(TAG, "fehlt added");
											}
											if (event1.event_id == 8) {
												fehltbereits = true;
											}

										} else {

											if (event1.event_id == 8) {
												if (event.event_id == 15) {
													fehltbereits = true;
												}
											}
											liste.add(event1);

											Log.e(TAG, "added");
										}
									}
								}
								if (fehltbereits == false) {
									liste.add(event);
								} else {
									Toast.makeText(
											getActivity()
													.getApplicationContext(),
											"SchuelerIn fehlt bereits.",
											Toast.LENGTH_SHORT).show();
								}

								JSONArray events1 = new JSONArray();
								for (int i = 0; i < liste.size(); i++) {
									Event currentevent = liste.get(i);
									Log.e(TAG,
											"currentevent: "
													+ currentevent.toString());
									JSONObject json1 = new JSONObject();
									json1.put("Zeitstempel", currentevent.date);
									json1.put("Zus_Wert", currentevent.Zus_Wert);
									json1.put("Ereignstyp_id",
											currentevent.event_id);
									json1.put("schueler_id",
											currentevent.schueler_id);
									json1.put("klasse_id",
											currentevent.klasse_id);
									events1.put(json1);
								}
								// Toast.makeText(getActivity().getApplicationContext(),events.toString(),
								// Toast.LENGTH_SHORT).show();
								PrintWriter out = new PrintWriter(
										new OutputStreamWriter(
												new FileOutputStream(fullname)));

								// Toast.makeText(getActivity(), "DID EXIST" +
								// events.toString(),Toast.LENGTH_SHORT).show();
								out.print(events1.toString());
								out.flush();
								out.close();
								Log.e(TAG,
										"out: " + events.toString());
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {
							try {
								PrintWriter out = new PrintWriter(
										new OutputStreamWriter(
												new FileOutputStream(fullname)));

								events = new JSONArray();
								events.put(0, json);

								// Toast.makeText(getActivity().getApplicationContext(),"DIDN'T EXIST"
								// +
								// events.toString(),Toast.LENGTH_SHORT).show();
								out.print(events.toString());
								out.flush();
								out.close();
								Log.e(TAG, events.toString());
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		return rootView;
	}
}
