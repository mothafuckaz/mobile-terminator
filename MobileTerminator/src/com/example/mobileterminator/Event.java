package com.example.mobileterminator;

import java.util.GregorianCalendar;

public class Event {

	int schueler_id;
	int event_id;
	int Zus_Wert;
	String date;
	String klasse_id;

	public Event(int schueler_id, int event_id, int zus_Wert, String date,
			String klasse_id) {
		super();
		this.schueler_id = schueler_id;
		this.event_id = event_id;
		Zus_Wert = zus_Wert;
		this.date = date;
		this.klasse_id = klasse_id;
	}

	@Override
	public String toString() {
		if (event_id == 1) {
			if (Zus_Wert == 1) {
				return "Mitarbeitsplus" + " - " + date;
			}
			if (Zus_Wert == -1) {
				return "Mitarbeitsminus" + " - " + date;
			}
		} else {
			if (event_id == 8) {
				return "SCHUELERIN FEHLT" + " - " + date;
			} else {
				if (event_id == 15) {
					return "ZU SPEAT: " + Zus_Wert + " Minuten" + " - " + date;
				}
			}
		}
		return schueler_id + " - " + event_id + ": " + Zus_Wert + "_" + date
				+ "_klasse:" + klasse_id;
	}

}
