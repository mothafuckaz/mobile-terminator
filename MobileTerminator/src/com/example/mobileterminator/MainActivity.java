package com.example.mobileterminator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.*;

import com.example.mobileterminator.R.layout;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	int count = 1;
	Schueler student;
	ArrayList<Schueler> students = new ArrayList<Schueler>();
	ArrayList<Klasse> klassenliste = new ArrayList<Klasse>();
	Spinner spinner;
	final public static String TAG = "Mobile Terminator";
	public static boolean start = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_main);
		start = false;
		readFile();
		// fillList();
		initSpinner();

	}

	private void initSpinner() {
		spinner = (Spinner) findViewById(R.id.KlassenauswahlSpinner);
		ArrayAdapter<Klasse> adapter = new ArrayAdapter<Klasse>(
				getApplicationContext(),
				android.R.layout.simple_dropdown_item_1line, klassenliste);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Klasse klasse = klassenliste.get(position);
				students = klasse.students;
				createTable();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		start = true;
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.v(TAG, "ONRESUMEmain!!");
		if (start == true) {
			Intent intent = new Intent(this, StartBildschirm.class);
			startActivity(intent);
		}
		super.onResume();
	}

	private void readFile() {
		JSONArray klassen;
		String s = "";
		String str;

		AssetManager assets = getAssets();

		String fullname = "Knuete_Export.js";
		try {
			java.io.BufferedReader reader = new java.io.BufferedReader(
					new java.io.InputStreamReader(assets.open(fullname)));
			while (reader.ready()) {
				str = reader.readLine().toString();
				s += str;
			}
			// Log.v(TAG, s + "");

			klassen = new org.json.JSONArray(s);

			for (int i = 0; i < klassen.length(); i++) {
				JSONObject klasse = klassen.getJSONObject(i);
				JSONObject klasseninfo = klasse.getJSONObject("klasse");
				String id = klasseninfo.getString("id");
				String name = klasseninfo.getString("name");
				String bezeichnung = klasseninfo.getString("bezeichnung");
				// Log.e(TAG, id + " " + name + " " + bezeichnung);
				JSONArray leistungen = klasse.getJSONArray("leistungen");
				students = new ArrayList<Schueler>();
				for (int j = 0; j < leistungen.length(); j++) {
					JSONObject sch = leistungen.getJSONObject(j);
					int schueler_id = sch.getInt("schueler_id");
					String katnr = sch.getString("katnr");
					String namesch = sch.getString("name");
					int fehlt = sch.getInt("fehlt");
					int fruehgewarnt = sch.getInt("fruehgewarnt");
					String note = sch.getString("note");
					int sumplus = sch.getInt("sumplus");
					int summinus = sch.getInt("summinus");
					double prozent = sch.getDouble("prozent");
					int muendlich = sch.getInt("muendlich");
					int muendlichplus = sch.getInt("muendlplus");
					int muendlichminus = sch.getInt("muendlminus");
					String semnote = sch.getString("semnote");
					students.add(new Schueler(schueler_id, katnr, id, namesch,
							fehlt, fruehgewarnt, note, sumplus, summinus,
							prozent, muendlichminus, muendlichplus,
							muendlichminus, semnote, null));
					// Log.e(TAG, students.get(j).toString());
				}
				Klasse schuelerklasse = new Klasse(id, name, bezeichnung,
						students);
				klassenliste.add(schuelerklasse);
				// Log.e(TAG, schuelerklasse.toString() + " hinzugefügt");

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void fillList() {
		for (int i = 0; i < 30; i++) {
			String name = "Max Mustermann";
			student = new Schueler(i, "i", "i", name, i, i, "i", i, i, i, i, i,
					i, "i", null);
			students.add(student);
			count++;
		}
		Klasse kla = new Klasse("1", "1a", "Informatik", students);
		klassenliste.add(kla);
	}

	private void createTable() {
		Button[] buttons = new Button[students.size()];
		TableLayout tl = (TableLayout) findViewById(R.id.TableLayout);
		tl.removeAllViewsInLayout();
		tl.invalidate();
		TableRow tr1 = new TableRow(this);
		tr1.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr2 = new TableRow(this);
		tr2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr3 = new TableRow(this);
		tr3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr4 = new TableRow(this);
		tr4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr5 = new TableRow(this);
		tr5.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr6 = new TableRow(this);
		tr6.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		TableRow tr7 = new TableRow(this);
		tr7.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		// Log.e(TAG, students.size() + "");
		for (int i = 0; i < students.size(); i++) {
			final Schueler sch = students.get(i);
			if (i <= 5) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr1.addView(button);
			}

			if (i >= 6 && i <= 11) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);

				// button.setBackgroundColor(R.drawable.custom_btn_black_pearl);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr2.addView(button);
			}

			if (i >= 12 && i <= 17) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				// button.setBackgroundColor(R.drawable.custom_btn_black_pearl);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr3.addView(button);
			}

			if (i >= 18 && i <= 23) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				// button.setBackgroundColor(R.drawable.custom_btn_black_pearl);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr4.addView(button);
			}

			if (i >= 24 && i <= 29) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				// button.setBackgroundColor(R.drawable.custom_btn_black_pearl);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr5.addView(button);
			}

			if (i >= 30 && i <= 35) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setBackgroundColor(R.drawable.custom_btn_black_pearl);
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr6.addView(button);
			}

			if (i >= 36 && i <= 40) {
				Button button = new Button(this);
				button.setText(shortname(students.get(i).name));
				// button.setId(students.get(i).schueler_id);
				button.setId(i);
				button.setPadding(0, 0, 0, 0);
				button.setTextAppearance(getApplicationContext(),
						R.style.button_text);
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent_info_event = new Intent(
								getApplicationContext(),
								Events_SchuelerInfo.class);
						Intent putExtra = intent_info_event.putExtra(
								"Schueler", sch);
						startActivity(intent_info_event);
					}
				});
				buttons[i] = button;
				tr7.addView(button);
			}

		}

		tl.addView(tr1, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr2, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr3, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr4, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr5, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr6, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.addView(tr7, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
	}

	private String shortname(String name) {
		String[] splitname = name.split(" ");
		String shortname = splitname[0] + " " + splitname[1].charAt(0) + ".";
		return shortname;
	}
}

/*






*/