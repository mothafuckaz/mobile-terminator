package com.example.mobileterminator;

import java.io.Serializable;
import java.util.ArrayList;

public class Schueler implements Serializable {

	public int schueler_id;
	public String katnr;
	public String klasse_id;
	public String name;
	public int fehlt;
	public int fruehgewarnt;
	public String note;
	public int sumplus;
	public int summinus;
	public double prozent;
	public int muendlich;
	public int muendlich_plus, muendlich_minus;
	public String semnote;
	ArrayList<Event> events;

	public Schueler(int schueler_id, String katnr, String klasse_id,
			String name, int fehlt, int fruehgewarnt, String note, int sumplus,
			int summinus, double prozent, int muendlich, int muendlich_plus,
			int muendlich_minus, String semnote, ArrayList<Event> events) {
		super();
		this.schueler_id = schueler_id;
		this.katnr = katnr;
		this.klasse_id = klasse_id;
		this.name = name;
		this.fehlt = fehlt;
		this.fruehgewarnt = fruehgewarnt;
		this.note = note;
		this.sumplus = sumplus;
		this.summinus = summinus;
		this.prozent = prozent;
		this.muendlich = muendlich;
		this.muendlich_plus = muendlich_plus;
		this.muendlich_minus = muendlich_minus;
		this.semnote = semnote;
		this.events = events;
	}

	public String getKlasse_id() {
		return klasse_id;
	}

	public void setKlasse_id(String klasse_id) {
		this.klasse_id = klasse_id;
	}

	public int getSchueler_id() {
		return schueler_id;
	}

	public void setSchueler_id(int schueler_id) {
		this.schueler_id = schueler_id;
	}

	public String getKatnr() {
		return katnr;
	}

	public void setKatnr(String katnr) {
		this.katnr = katnr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFehlt() {
		return fehlt;
	}

	public void setFehlt(int fehlt) {
		this.fehlt = fehlt;
	}

	public int getFruehgewarnt() {
		return fruehgewarnt;
	}

	public void setFruehgewarnt(int fruehgewarnt) {
		this.fruehgewarnt = fruehgewarnt;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getSumplus() {
		return sumplus;
	}

	public void setSumplus(int sumplus) {
		this.sumplus = sumplus;
	}

	public int getSumminus() {
		return summinus;
	}

	public void setSumminus(int summinus) {
		this.summinus = summinus;
	}

	public double getProzent() {
		return prozent;
	}

	public void setProzent(double prozent) {
		this.prozent = prozent;
	}

	public int getMuendlich() {
		return muendlich;
	}

	public void setMuendlich(int muendlich) {
		this.muendlich = muendlich;
	}

	public int getMuendlich_plus() {
		return muendlich_plus;
	}

	public void setMuendlich_plus(int muendlich_plus) {
		this.muendlich_plus = muendlich_plus;
	}

	public int getMuendlich_minus() {
		return muendlich_minus;
	}

	public void setMuendlich_minus(int muendlich_minus) {
		this.muendlich_minus = muendlich_minus;
	}

	public String getSemnote() {
		return semnote;
	}

	public void setSemnote(String semnote) {
		this.semnote = semnote;
	}

	public ArrayList<Event> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<Event> events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "Schueler [schueler_id=" + schueler_id + ", katnr=" + katnr
				+ ", name=" + name + ", fehlt=" + fehlt + ", fruehgewarnt="
				+ fruehgewarnt + ", note=" + note + ", sumplus=" + sumplus
				+ ", summinus=" + summinus + ", prozent=" + prozent
				+ ", muendlich=" + muendlich + ", muendlich_plus="
				+ muendlich_plus + ", muendlich_minus=" + muendlich_minus
				+ ", semnote=" + semnote + ", events=" + events + "]";
	}

}
