package com.example.mobileterminator;

import android.app.*;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.*;
import android.text.Editable;
import android.view.*;
import android.widget.*;

public class StartBildschirm extends Activity {

	EditText passwordeingabe;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.startbildschirm_layout);
		passwordeingabe = (EditText) findViewById(R.id.password);
		passwordeingabe.setText("");
	}

	public void passwordeingabe(View view) {
		if (passwordeingabe.getText().toString().equals("")) {
		    Toast.makeText(this, "Es wurden kein Passwort eingegeben!", Toast.LENGTH_SHORT).show();
		    return;
		}
		String eingabe = passwordeingabe.getText().toString();
		if (Integer.parseInt(eingabe) == 12003) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		} else {
			Toast.makeText(this, "FALSCHE EINGABE", Toast.LENGTH_LONG).show();
		}
	}
}
