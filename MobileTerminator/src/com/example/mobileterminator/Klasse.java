package com.example.mobileterminator;

import java.util.ArrayList;

public class Klasse {

	String id;
	String name;
	String bezeichnung;
	ArrayList<Schueler> students = new ArrayList<Schueler>();

	public Klasse(String id, String name, String bezeichnung,
			ArrayList<Schueler> students) {
		super();
		this.id = id;
		this.name = name;
		this.bezeichnung = bezeichnung;
		this.students = students;
	}

	@Override
	public String toString() {
		return name;
	}

}
